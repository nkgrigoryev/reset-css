const path = require('path');

module.exports = {
    entry: './',
    output: {
        path: path.resolve(__dirname, 'demo'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    mode: 'development'
};